# HOW TO USE THIS IMPLEMENTATION

This implementation of the `paging-mission-control` challenge was created for the Java JVM.
It uses Gradle for building as the build configuration framework and JDK 11.

## To Build

Make sure the version of the Java JDK you are using when running `java -version` and `javac -version` is JDK 11.

Once that's verified, simply run:
```
./gradlew clean build
```
at the top level. This uses the Gradle wrapper, so you don't need to
have Gradle installed.

## To Run the Tests

If just wanted to verify the tests work, run:
```
./gradlew test
```

## To Execute Application

After building the project, the jar `build/libs/telemetryApp.jar` should have been
created.  You will need to pass an input file, formatted per the spec for this challenge.
See README.md for the format.  There is also a sample input.txt file under
`src/test/resources/input.txt`: 
```
java -jar build/libs/telemetryApp.jar src/test/resources/input.txt
```

Note that there is an optional JVM property for setting an alternate configuration for
the application.  The default configuration can be found in `src/main/resources/application.properties`
and it looks something like this:
```
telemetry.alertTrackerTypes=TSTAT:RED_HIGH,BATT:RED_LOW
telemetry.alertIntervalMinutes=5
telemetry.inputDatePattern=yyyyMMdd HH:mm:ss.SSS
telemetry.outputDateFormat=yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
```

To provide an alternate configuration, include this argument in the command `-DconfigFile=<alternate-config>`:
```
java -DconfigFile=<some-config-file> -jar build/libs/telemetryApp.jar src/test/resources/input.txt
```

