package telemetry;

import org.junit.jupiter.api.Test;
import telemetry.data.TelemetryData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class AlertTrackerTest {

    @Test
    void add() {
        AlertTracker alertTracker = new AlertTracker("component", "severity", 5);
        TelemetryData data = new TelemetryData.TelemetryDataBuilder("20180101 23:01:05.001",
                "1",
                "101",
                "98",
                "25",
                "20",
                "99.9",
                "TSTAT").setDatePattern("yyyyMMdd HH:mm:ss.SSS").build();
        alertTracker.add(data);
        assertTrue(alertTracker.hasRecord("1", getDateFromString("20180101 23:01:05.001")));
        //not a satellite id that has been added
        assertFalse(alertTracker.hasRecord("2", getDateFromString("20180101 23:01:05.001")));
        //not a record that has been added
        assertFalse(alertTracker.hasRecord("1", getDateFromString("20180202 23:01:05.001")));

        //adding another telemetry record
        TelemetryData data2 = new TelemetryData.TelemetryDataBuilder("20180101 23:02:05.001",
                "1",
                "101",
                "98",
                "25",
                "20",
                "99.9",
                "TSTAT").setDatePattern("yyyyMMdd HH:mm:ss.SSS").build();
        alertTracker.add(data2);
        assertTrue(alertTracker.hasRecord("1", getDateFromString("20180101 23:02:05.001")));

        //adding another telemetry record which triggers alert and other two previous will be removed
        TelemetryData data3 = new TelemetryData.TelemetryDataBuilder("20180101 23:03:05.001",
                "1",
                "101",
                "98",
                "25",
                "20",
                "99.9",
                "TSTAT").setDatePattern("yyyyMMdd HH:mm:ss.SSS").build();
        alertTracker.add(data3);
        assertTrue(alertTracker.hasAlerts());
        assertTrue(alertTracker.hasRecord("1", getDateFromString("20180101 23:03:05.001")));
        //check for previous records which should now be gone
        assertFalse(alertTracker.hasRecord("1", getDateFromString("20180101 23:02:05.001")));
        assertFalse(alertTracker.hasRecord("1", getDateFromString("20180101 23:01:05.001")));
    }

    Date getDateFromString(String timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.ENGLISH);
        Date date = null;
        try {
            date = formatter.parse(timestamp);
        } catch (ParseException e) {
            // Not concerned about his exception here
        }
        return date;
    }
}