package telemetry.data;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class AlertTrackerDataTest {

    @ParameterizedTest(name="{0} : Is \"{2}\" within 5 min range")
    @MethodSource("dateRangeProvider")
    void isWithinRange(String id, String startDate, String newDate, boolean result) {
        AlertTrackerData trackerData = new AlertTrackerData(id);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.ENGLISH);
        try {
            trackerData.addRecord(formatter.parse(startDate));
            trackerData.computeEndRange(5);
            assertEquals(trackerData.isWithinRange(formatter.parse(newDate)), result);
        } catch (ParseException e) {
            //Not concerned about this Exception here
        }
    }

    @ParameterizedTest(name="{0} : \"{2}\" is {3} minutes after \"{1}\"")
    @MethodSource("dateRangeMinutesProvider")
    void computeEndRange(String id, String startDate, String newDate, int minutes) {
        AlertTrackerData trackerData = new AlertTrackerData(id);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.ENGLISH);
        try {
            trackerData.addRecord(formatter.parse(startDate));
            trackerData.computeEndRange(minutes);
            assertEquals(trackerData.getEndRange(), formatter.parse(newDate));
        } catch (ParseException e) {
            //Not concerned about this exception here
        }
    }

    static Stream<Arguments> dateRangeProvider() {
        return Stream.of(
            arguments("1", "20180101 23:01:05.001", "20180101 23:04:05.001", true),
            arguments("2", "20180101 23:01:05.001", "20180101 23:06:05.001", true),
            arguments("3", "20180101 23:01:05.001", "20180101 23:06:06.001", false),
            arguments("4", "20180101 23:05:05.001", "20180101 23:14:05.001", false)
        );
    }

    static Stream<Arguments> dateRangeMinutesProvider() {
        return Stream.of(
            arguments("1", "20180101 23:01:05.001", "20180101 23:06:05.001", 5),
            arguments("2", "20180101 23:01:05.001", "20180101 23:08:05.001", 7),
            arguments("3", "20180101 23:01:05.001", "20180101 23:02:05.001", 1),
            arguments("4", "20180101 23:01:05.001", "20180101 23:01:05.001", 0)
        );
    }

}