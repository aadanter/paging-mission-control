package telemetry;

import org.junit.jupiter.api.Test;
import telemetry.data.TelemetryData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TelemetryEvaluatorTest {

    @Test
    void evaluate() {
        Map<String, AlertTracker> alertTrackers = new HashMap<>();
        AlertTracker alertTracker = new AlertTracker("TSTAT", "high", 5);
        alertTrackers.put("TSTAT", alertTracker);
        TelemetryEvaluator evaluator = new TelemetryEvaluator(alertTrackers);
        TelemetryData data1 = new TelemetryData.TelemetryDataBuilder("20180101 23:01:05.001",
                "1",
                "101",
                "98",
                "25",
                "20",
                "120.9",
                "TSTAT").setDatePattern("yyyyMMdd HH:mm:ss.SSS").build();
        TelemetryData data2 = new TelemetryData.TelemetryDataBuilder("20180101 23:03:00.001",
                "1",
                "101",
                "98",
                "25",
                "20",
                "99.9",
                "TSTAT").setDatePattern("yyyyMMdd HH:mm:ss.SSS").build();
        TelemetryData data3 = new TelemetryData.TelemetryDataBuilder("20180101 23:05:05.001",
                "1",
                "101",
                "98",
                "25",
                "20",
                "50",
                "BATT").setDatePattern("yyyyMMdd HH:mm:ss.SSS").build();

        List<TelemetryData> entries = new ArrayList<>();
        entries.add(data1);
        entries.add(data2);
        entries.add(data3);

        evaluator.evaluate(entries);
        assertTrue(alertTracker.hasRecord( "1", getDateFromString("20180101 23:01:05.001")));
        assertFalse(alertTracker.hasRecord( "1", getDateFromString("20180101 23:03:00.001")));
        assertFalse(alertTracker.hasRecord( "1", getDateFromString("20180101 23:05:05.001")));
    }

    Date getDateFromString(String timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.ENGLISH);
        Date date = null;
        try {
            date = formatter.parse(timestamp);
        } catch (ParseException e) {
            // Not concerned about his exception here
        }
        return date;
    }
}