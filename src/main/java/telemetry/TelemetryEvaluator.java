package telemetry;

import telemetry.data.TelemetryAlert;
import telemetry.data.TelemetryData;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The TelemetryEvaluator class tests for whether a telemetry event is an
 * alert event. If it is, the event is added to the respective alertTracker.
 */
public class TelemetryEvaluator {

    private Map<String, AlertTracker> trackers;

    /**
     * Constructor
     *
     * @param trackers Map containing AlertTracker objects.
     */
    public TelemetryEvaluator(Map<String, AlertTracker> trackers) {
        this.trackers = trackers;
    }

    /**
     * Evaluates collection of TelemetryData and returns alerts.
     *
     * @param data List of TelemetryData
     *
     * @return List of TelemetryAlerts
     */
    public List<TelemetryAlert> evaluate(List<TelemetryData> data) {
        for (TelemetryData record : data) {
            if (record.getComponent().equals("TSTAT")) {
                if (record.getRawValue() > record.getRedHighLimit()) {
                    trackers.get("TSTAT").add(record);
                }
            } else if (record.getComponent().equals("BATT")) {
                if (record.getRawValue() < record.getRedLowLimit()) {
                    trackers.get("BATT").add(record);
                }
            }
        }

        return trackers.entrySet().stream()
                .filter(t -> t.getValue().hasAlerts())
                .map( e -> e.getValue().getAlerts())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}
