package telemetry;

import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

/**
 * Class used to read and hold application configuration.  The configuration may be
 * read from a file in the classpath or from a file on the filesystem.
 */
public class TelemetryProperties {

    static final int DEFAULT_ALERT_INTERVAL_MINUTES = 10;

    private final String configFile;
    private String[] alertTrackerTypes = null;
    private int alertIntervalMinutes = 0;
    private String inputDatePattern = null;
    private String outputDateFormat = null;

    /**
     * Constructor
     *
     * @param configFile path to configuration file
     */
    public TelemetryProperties(String configFile) {
        this.configFile = configFile;
    }

    /**
     * Read properties from file in classpath.
     *
     * @throws IOException
     */
    public void readProperties() throws IOException {
        InputStream input = getClass().getClassLoader().getResourceAsStream(configFile);
        readProperties(input);
    }

    /**
     * Read properties from file on filesystem.
     *
     * @param file File on filesystem
     *
     * @throws IOException
     */
    public void readProperties(File file) throws IOException {
        InputStream input = new FileInputStream(file);
        readProperties(input);
    }

    /**
     * Return value of `telemetry.alertTrackerTypes`
     *
     * @return alertTrackerTypes
     */
    public String[] getAlertTrackerTypes() {
        return alertTrackerTypes;
    }

    /**
     * Return value of `telemetry.alertIntervalMinutes`
     *
     * @return alertIntervalMinutes
     */
    public int getAlertIntervalMinutes() {
        return alertIntervalMinutes;
    }

    /**
     * Return value of `telemetry.inputDatePattern`
     *
     * @return inputDatePattern
     */
    public String getInputDatePattern() {
        return inputDatePattern;
    }

    /**
     * Return value of `telemetry.outputDateFormat`
     *
     * @return outputDateFormat
     */
    public String getOutputDateFormat() {
        return outputDateFormat;
    }

    private void readProperties(InputStream input) throws IOException {
        Properties props = new Properties();
        if (input != null) {
            props.load(input);
        } else {
            throw new FileNotFoundException();
        }
        readAlertTypes(props);
        readAlertIntervalMinutes(props);
        readInputDatePattern(props);
        readOutputDateFormat(props);
    }


    private void readAlertTypes(Properties props) {
        String typesString = props.getProperty("telemetry.alertTrackerTypes");
        if (typesString != null) {
            alertTrackerTypes =  typesString.split(",");
        }
    }

    private void readAlertIntervalMinutes(Properties props) {
        String intervalString = props.getProperty("telemetry.alertIntervalMinutes");
        if (intervalString != null) {
            alertIntervalMinutes = Integer.parseInt(intervalString);
        } else {
            alertIntervalMinutes = DEFAULT_ALERT_INTERVAL_MINUTES;
        }
    }

    private void readInputDatePattern(Properties props) {
        inputDatePattern = props.getProperty("telemetry.inputDatePattern");
    }

    private void readOutputDateFormat(Properties props) {
        outputDateFormat = props.getProperty("telemetry.outputDateFormat");
    }
}
