package telemetry;

import telemetry.data.TelemetryData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class encapsulating how to read raw telemetry events.
 */
public class TelemetryReader {

    private final ArrayList<TelemetryData> telemetryData = new ArrayList<>();
    private String datePattern;

    /**
     * Return list of TelemetryData
     *
     * @return list of TelemetryData
     */
    public ArrayList<TelemetryData> getTelemetryData() {
        return telemetryData;
    }

    //test??
    /**
     * Reads input file into list of TelemetryData
     */
    public void read(String telemetrySource) {
        try (BufferedReader buffer = new BufferedReader(new FileReader(telemetrySource))){
            String line;
            while ((line = buffer.readLine()) != null) {
                telemetryData.add(convertLineToData(line));
            }
        } catch (IOException e) {
            System.out.printf("Error encountered trying to read file %s%n", telemetrySource);
        }
    }

    /**
     * Set pattern for reading timestamps from telemetry events.
     *
     * @param pattern
     */
    public void setDatePattern(String pattern) {
        datePattern = pattern;
    }

    //Creates TelemetryData object from raw telemetry event
    private TelemetryData convertLineToData(String record) {
        String[] recordTokens = record.split("\\|");
        TelemetryData.TelemetryDataBuilder dataBuilder = new TelemetryData.TelemetryDataBuilder(
                recordTokens[0],
                recordTokens[1],
                recordTokens[2],
                recordTokens[3],
                recordTokens[4],
                recordTokens[5],
                recordTokens[6],
                recordTokens[7]).setDatePattern(datePattern);
        return dataBuilder.build();
    }
}
