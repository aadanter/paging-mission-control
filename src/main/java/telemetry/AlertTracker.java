package telemetry;

import telemetry.data.AlertTrackerData;
import telemetry.data.TelemetryAlert;
import telemetry.data.TelemetryData;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The AlertTracker class handles the tracking of alerts and potential alert events
 * for a specific component and a specific severity. Currently, if 3 potential alert
 * events are detected during a configurable interval an alert is generated.
 */
public class AlertTracker {

    private final Integer rangeMinutes;
    private final String component;
    private final String severity;
    private final Map<String, AlertTrackerData> tracker;
    private final List<TelemetryAlert> alerts;

    private Boolean hasAlerts = false;

    /**
     * Constructor
     *
     * @param component The component the alerts correspond to
     * @param severity The severity for the alerts
     * @param rangeMinutes The interval over which the alerts are being tracked
     */
    public AlertTracker(String component, String severity, int rangeMinutes) {
        this.component = component;
        this.severity = severity;
        this.rangeMinutes = rangeMinutes;
        this.tracker = new HashMap<>();
        this.alerts = new ArrayList<>();
    }

    /**
     * Add alert record to tracker data.
     *
     * @param data TelemetryData being added
     */
    public void add(TelemetryData data) {
        String satelliteId = data.getSatelliteId();
        if (!tracker.containsKey(satelliteId)) {
            tracker.put(satelliteId, new AlertTrackerData(satelliteId));
        }
        insertRecord(satelliteId, data.getTimestamp());
    }

    /**
     * Returns alerts
     * @return List of TelemetryAlert objects
     */
    public List<TelemetryAlert> getAlerts() {
        return alerts;
    }

    /**
     * Returns component
     * @return component
     */
    public String getComponent() {
        return component;
    }

    /**
     * Returns severity
     * @return severity
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Returns whether AlertTracker has alerts
     * @return true if AlertTracker has alerts, false otherwise
     */
    public boolean hasAlerts() {
        return hasAlerts;
    }

    public boolean hasRecord(String id, Date timestamp) {
        AlertTrackerData data = tracker.get(id);
        if (data != null) {
            return data.hasRecord(timestamp);
        } else {
            return false;
        }
    }
    /**
     *  Creates TelemetryAlert for combination of Satellite ID and timestamp.
     * @param id Satellite ID
     * @param timestamp Timestamp of data
     * @return TelemetryAlert
     */
    private TelemetryAlert convertToAlert(String id, Date timestamp) {
        return new TelemetryAlert(id, severity, component, timestamp);
    }

    private void insertRecord(String satId, Date newTimestamp) {
        int recordCount = tracker.get(satId).getNumberOfRecords();
        if (recordCount == 0) {
            tracker.get(satId).addRecord(newTimestamp);
            tracker.get(satId).computeEndRange(rangeMinutes);
        } else if (recordCount == 1) {
            if (tracker.get(satId).isWithinRange(newTimestamp)) {
                tracker.get(satId).addRecord(newTimestamp);
            } else {
                tracker.get(satId).clearRecords();
                tracker.get(satId).addRecord(newTimestamp);
                tracker.get(satId).computeEndRange(rangeMinutes);
            }
        } else if (recordCount == 2) {
            if (tracker.get(satId).isWithinRange(newTimestamp)) {
                alerts.add(convertToAlert(satId, tracker.get(satId).getStartRange()));
                hasAlerts = true;
            }
            tracker.get(satId).clearRecords();
            tracker.get(satId).addRecord(newTimestamp);
            tracker.get(satId).computeEndRange(rangeMinutes);
        }
    }
}
