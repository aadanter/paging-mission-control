package telemetry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import telemetry.data.TelemetryAlert;
import telemetry.data.TelemetryData;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The TelemetryApp class is the driver class for the `telemetryApp`.  It handles
 * the initialization of the reader, evaluator and alertTracker objects used to check
 * for alerts in a stream of telemetry events.
 */
public class TelemetryApp {

    static final String APP_USAGE = "Usage: java -jar telemetryApp.jar <file with telemetry data>";
    static final String CONFIGURATION_FILE = "application.properties";

    private final TelemetryReader reader;
    private final TelemetryEvaluator evaluator;
    private final String outputDateFormat;

    /**
     * Constructor
     * @param reader object for reading telemetry events
     * @param evaluator object used in the evaluation of the telemetry events
     * @param config application configuration
     */
    public TelemetryApp(TelemetryReader reader,
                        TelemetryEvaluator evaluator,
                        TelemetryProperties config) {
        this.reader = reader;
        this.evaluator = evaluator;
        this.outputDateFormat = config.getOutputDateFormat();
    }

    /**
     * TelemetryApp entrypoint.  Prints alerts retrieved from TelemetryEvaluator to console.
     */
    public void start() {
        List<TelemetryData> inputData = reader.getTelemetryData();
        List<TelemetryAlert> alerts = evaluator.evaluate(inputData);
        Gson gson = new GsonBuilder()
                .setDateFormat(outputDateFormat)
                .setPrettyPrinting().create();
        System.out.println(gson.toJson(alerts));
    }

    /**
     * TelemetryApp main method.  This sets up the objects which are passed into
     * TelemetryApp.
     *
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println(APP_USAGE);
        } else {
            String userConfig = System.getProperty("configFile");
            TelemetryProperties config = null;
            try {
                if (userConfig != null) {
                    config = new TelemetryProperties(userConfig);
                    File configFile = new File(userConfig);
                    config.readProperties(configFile);
                } else {
                    config = new TelemetryProperties(CONFIGURATION_FILE);
                    config.readProperties();
                }
            } catch (IOException e) {
                System.out.println("Error: Unable to read configuration file.");
                System.exit(1);
            }

            String[] trackers = config.getAlertTrackerTypes();
            int alertInterval = config.getAlertIntervalMinutes();
            String readDatePattern = config.getInputDatePattern();

            TelemetryReader reader = new TelemetryReader();
            reader.setDatePattern(readDatePattern);
            reader.read(args[0]);

            Map<String, AlertTracker> alertTrackers = new HashMap<>();
            for (String trackerType : trackers) {
                String[] trackerTypeToks = trackerType.split(":");
                String component = trackerTypeToks[0];
                String severity = trackerTypeToks[1].replace("_", " ");
                AlertTracker alertTracker = new AlertTracker(component, severity, alertInterval);
                alertTrackers.put(component, alertTracker);
            }

            TelemetryEvaluator evaluator = new TelemetryEvaluator(alertTrackers);
            TelemetryApp app = new TelemetryApp(reader, evaluator, config);
            app.start();
        }
    }
}
