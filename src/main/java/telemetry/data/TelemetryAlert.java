package telemetry.data;

import java.util.Date;

/**
 * TelemetryAlert POJO to make serialization to JSON easier.
 */
public class TelemetryAlert {
    private final String satelliteId;
    private final String severity;
    private final String component;
    private final Date timestamp;

    public TelemetryAlert(String satelliteId, String severity, String component, Date timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component =  component;
        this.timestamp = timestamp;
    }
}
