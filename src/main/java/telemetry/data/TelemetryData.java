package telemetry.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * The TelemetryData class represents one telemetry record read from input. It includes a builder which
 * handles conversion of data as needed prior to building TelemetryData object.
 */
public class TelemetryData {
    private Date timestamp = null;
    private String satelliteId = null;
    private Integer redHighLimit = null;
    private Integer yellowHighLimit = null;
    private Integer redLowLimit = null;
    private Integer yellowLowLimit = null;
    private Double rawValue = null;
    private String component = null;

    /**
     * The timestamp for telemetry record.
     *
     * @return timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * The satellite ID for telemetry record.
     *
     * @return satelliteId
     */
    public String getSatelliteId() {
        return satelliteId;
    }

    /**
     * The red high limit for telemetry record.
     *
     * @return redHighLimit
     */
    public Integer getRedHighLimit() {
        return redHighLimit;
    }

    /**
     * The yellow high limit for telemetry record.
     *
     * @return yellowHighLimit
     */
    public Integer getYellowHighLimit() {
        return yellowHighLimit;
    }

    /**
     * The red low limit for telemetry record.
     *
     * @return redLowLimit
     */
    public Integer getRedLowLimit() {
        return redLowLimit;
    }

    /**
     * The yellow low limit for telemetry record.
     * @return yellowLowLimit
     */
    public Integer getYellowLowLimit() {
        return yellowLowLimit;
    }

    /**
     * The raw data value for telemetry record.
     * @return rawValue
     */
    public Double getRawValue() {
        return rawValue;
    }

    /**
     * Name of component for telemetry record.
     *
     * @return component
     */
    public String getComponent() {
        return component;
    }

    private TelemetryData(TelemetryDataBuilder builder) {
        this.timestamp = builder.timestamp;
        this.satelliteId = builder.satelliteId;
        this.redHighLimit = builder.redHighLimit;
        this.yellowHighLimit = builder.yellowHighLimit;
        this.redLowLimit = builder.redLowLimit;
        this.yellowLowLimit = builder.yellowLowLimit;
        this.rawValue = builder.rawValue;
        this.component = builder.component;
    }

    /**
     * TelemetryDataBuilder handles the creation of TelemetryData instances.
     */
    public static class TelemetryDataBuilder {
        private final String stringTimestamp;
        private final String satelliteId;
        private final Integer redHighLimit;
        private final Integer yellowHighLimit;
        private final Integer redLowLimit;
        private final Integer yellowLowLimit;
        private final Double rawValue;
        private final String component;
        private Date timestamp;
        private String datePattern;

        /**
         * Set date pattern used when converting a timestamp string to a Date object.
         *
         * @param pattern The timestamp pattern
         *
         * @return Instance of TelemetryDataBuilder
         */
        public TelemetryDataBuilder setDatePattern(String pattern) {
            this.datePattern = pattern;
            return this;
        }

        /**
         * Constructor
         */
        public TelemetryDataBuilder(String timestamp, String satelliteId, String redHighLimit, String yellowHighLimit,
                                    String redLowLimit, String yellowLowLimit, String rawValue, String component) {

            this.stringTimestamp = timestamp;
            this.satelliteId = satelliteId;
            this.redHighLimit = Integer.parseInt(redHighLimit);
            this.yellowHighLimit = Integer.parseInt(yellowHighLimit);
            this.redLowLimit = Integer.parseInt(redLowLimit);
            this.yellowLowLimit = Integer.parseInt(yellowLowLimit);
            this.rawValue = Double.parseDouble(rawValue);
            this.component = component;
        }

        /**
         * Create TelemetryData from TelemetryDataBuilder.
         *
         * @return TelemetryData instance
         */
        public TelemetryData build() {
            this.timestamp = convertStringToDate(stringTimestamp);
            return new TelemetryData(this);
        }

        private Date convertStringToDate(String dateString) {
            SimpleDateFormat formatter = new SimpleDateFormat(datePattern, Locale.ENGLISH);
            Date timestamp;
            try {
                timestamp = formatter.parse(dateString);
            } catch (ParseException e) {
                return null;
            }
            return timestamp;
        }
    }
}
