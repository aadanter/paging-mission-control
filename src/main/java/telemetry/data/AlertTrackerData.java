package telemetry.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AlerTrackerData encapsulates alert data for a specific satellite.
 */
public class AlertTrackerData {
    private final String satelliteId;
    private final List<Date> records;
    private Date endRange;

    /**
     * Constructor
     * @param id Satellite ID
     */
    public AlertTrackerData(String id) {
        satelliteId = id;
        records = new ArrayList<>();
        endRange = null;
    }

    /**
     * Returns how many alert records the object has.
     *
     * @return number of alert records
     */
    public int getNumberOfRecords() {
        return records.size();
    }

    /**
     * Returns the current end of alert interval.
     *
     * @return end timestamp as Date object
     */
    public Date getEndRange() {
        return endRange;
    }

    /**
     * Returns the current start of alert interval.
     *
     * @return start timestamp as Date object
     */
    public Date getStartRange() {
        if (records.size() > 0) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Returns ID of satellite data pertains to.
     *
     * @return satellite ID
     */
    public String getSatelliteId() {
        return satelliteId;
    }

    /**
     * Add alert record to internal list
     *
     * @param timestamp Date being added
     */
    public void addRecord(Date timestamp) {
        records.add(timestamp);
    }

    /**
     * Clear all records.  Once an alert is triggered, it's necessary to start
     * with a clean slate.
     */
    public void clearRecords() {
        records.clear();
    }

    /**
     * Calculate the end of range and update internally. At least one record must be
     * present in the internal list, otherwise this is a no op.
     *
     * @param minutes
     */
    public void computeEndRange(Integer minutes) {
        if (records.size() == 1) {
            endRange = new Date(records.get(0).getTime() + minutes * 60 * 1000L);
        }
    }

    /**
     * Checks if timestamp is between the start and end of a time range. The endpoints are
     * the first record in the internal list and the endRange that has been calculated.
     *
     * @param timestamp Date being checked
     *
     * @return true if wihin time range, false otherwise
     */
    public boolean isWithinRange(Date timestamp) {
        if (records.size() > 0) {
            return !(timestamp.before(records.get(0)) || timestamp.after(endRange));
        } else {
            return false;
        }
    }

    /**
     * Returns whether object has given timestamp in its records.
     *
     * @param timestamp Date being checked
     *
     * @return true if timestamp is present, false otherwise
     */
    public boolean hasRecord(Date timestamp) {
        return records.contains(timestamp);
    }
}
